const express = require('express');
const app = express();

app.get('/day1', (req, res, next) => {
  res.send('day1');
});
app.listen(3000, () => {
  console.log(`Server running port ${3000}`);
});
